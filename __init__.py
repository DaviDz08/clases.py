class Perro:


  especie = 'mamifero'

  def __init__(self, nombre, edad):
     self.nombre =nombre
     self.edad = edad

  def descripcion(self): 
     return "{} es {} años de edad".format(self.nombre, self.edad)

  def hablar(self, sonido):
     return "{} dice {}".format(self.nombre, sonido)

class Rottweiler(Perro):
   def correr(self, velocidad):
      print("{} correr a {}".format(self.nombre, velocidad))


lassie = Perro("Lassie", 5)
print(lassie.descripcion())
print(lassie.hablar("woof woof"))
roco =Rottweiler("Roco", 10)
roco.correr(50) 
print(roco.hablar("que lo que eh"))